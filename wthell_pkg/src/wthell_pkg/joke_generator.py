# joke_generator.py
"""
Module for generating jokes and plotting hearts based on mood.
"""

import os
import pyjokes
import matplotlib.pyplot as plt
import numpy as np
import random

def get_joke(mood, save_path=None):
    """
    Get a joke based on the provided mood.

    Parameters:
    - mood (str): The mood for which the joke should be generated.
    - save_path (str): The path to save the PDF file. If None, the plot is displayed without saving.

    Returns:
    - str: A joke corresponding to the given mood.
    """
    if mood.lower() == 'happy':
        os.makedirs(save_path, exist_ok=True)  # Ensure the directory exists
        plot_heart(os.path.join(save_path, 'heart_plot.pdf'))
        return get_random_joke()
    elif mood.lower() == 'sad':
        return "Why don't scientists trust atoms? Because they make up everything!"
    else:
        return "I'm sorry, I can't generate a joke for that mood."

def get_random_joke():
    """
    Get a random joke from a random category.

    Returns:
    - str: A random joke.
    """
    categories = ['neutral', 'chuck', 'all']
    category = random.choice(categories)
    return pyjokes.get_joke(category=category)

def plot_heart(save_path=None):
    """
    Plot a heart shape using Matplotlib and save it as a PDF.

    Parameters:
    - save_path (str): The path to save the PDF file. If None, the plot is displayed without saving.
    """
    t = np.linspace(0, 2 * np.pi, 100)
    x = 16 * np.sin(t)**3
    y = 13 * np.cos(t) - 5 * np.cos(2*t) - 2 * np.cos(3*t) - np.cos(4*t)
    
    plt.plot(x, y, color='red')
    plt.axis('off')

    if save_path:
        plt.savefig(save_path, format='pdf')
        print(f"Heart plot saved as {save_path}")
    else:
        plt.show()
