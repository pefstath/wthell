# main_script.py
from wthell_pkg.joke_generator import get_joke

def main():
    mood = "sad"  # You can replace this with input("Enter your mood: ") for user input
    
    # Provide the save path directly for "sad" mood
    save_path = "/g/scb/zaugg/pefstath/EBI_course/wthell/wthell_pkg/output/plots"

    joke = get_joke(mood, save_path)
    print(joke)

if __name__ == "__main__":
    main()
