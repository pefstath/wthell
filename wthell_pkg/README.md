# wthell_pkg

A package for doing great things!

## Installation

```bash
$ pip install wthell_pkg
```

## Usage

- TODO

## Contributing

Interested in contributing? Check out the contributing guidelines. Please note that this project is released with a Code of Conduct. By contributing to this project, you agree to abide by its terms.

## License

`wthell_pkg` was created by Evi P. Vlachou. It is licensed under the terms of the MIT license.

## Credits

`wthell_pkg` was created with [`cookiecutter`](https://cookiecutter.readthedocs.io/en/latest/) and the `py-pkgs-cookiecutter` [template](https://github.com/py-pkgs/py-pkgs-cookiecutter).
